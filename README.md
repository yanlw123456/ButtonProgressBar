#  ButtonProgressBar

#### 项目介绍
- 项目名称：ButtonProgressBar
- 所属系列：openharmony的第三方组件适配移植
- 功能：下载进度条，能显示下载进度以及下载完成的状态
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio 2.2 Beta2
- 基线版本：Releases 1.0

#### 效果演示
<img src="img/ButtonProgressBar2.gif"></img>

#### 安装教程


1.在项目根目录下的build.gradle文件中，

```
allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/releases/'
        }
    }
}
```

2.在entry模块的build.gradle文件中，

```
 dependencies {
    implementation('com.gitee.chinasoft_ohos:ButtonProgressBar:1.0.0')
    ......  
 }
```

在sdk6，DevEco Studio 2.2 Beta2下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明

1.将仓库导入到本地仓库中

2.在布局文件中加入ButtonProgressBar控件,代码实例如下:

```
    <github.ishaan.buttonprogressbar.ButtonProgressBar
           ohos:id="$+id:cl_main"
           ohos:height="50vp"
           ohos:left_margin="20vp"
           ohos:right_margin="20vp"
           ohos:bottom_margin="20vp"
           ohos:layout_alignment="bottom"
           ohos:width="match_parent"
           app:textColor="#ffffff"
           app:textSize="54px"
           app:type="1"
           apps:text="Upload"/>

```
java代码调用示例：
```
     mLoader = (ButtonProgressBar) findComponentById(ResourceTable.Id_cl_main);
            mLoader.setClickedListener(this);

  @Override
    public void onClick(Component component) {
        if (mLoader.getLoaderType() == ButtonProgressBar.Type.DETERMINATE) {
            callHandler();
        } else {
            mLoader.startLoader();
            long param = 0L;
            Object object = null;
            InnerEvent delayInnerEvent = InnerEvent.get(EVENT_MESSAGE_DELAY_2, param, object);
            handler.sendEvent(delayInnerEvent, DELAY_TIME_2, EventHandler.Priority.IMMEDIATE);
        }
    }

    public void updateUI(int type) {
        switch (type){
            case EVENT_MESSAGE_DELAY_1:
                getUITaskDispatcher().asyncDispatch(() -> mLoader.setProgress(progress));
                break;
            case EVENT_MESSAGE_DELAY_2:
                getUITaskDispatcher().asyncDispatch(new Runnable() {
                    @Override
                    public void run() {
                        mLoader.stopLoader();
                        Toast.show(getContext(),"Complete");
                    }
                });
                break;
        }
    }
```


#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

#### 版本迭代

- 1.0.0

#### 版权和许可信息

```
    Copyright 2017 Ishaan Kakkar

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

```